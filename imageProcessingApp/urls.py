"""imageProcessingApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.conf import settings
from django.conf.urls.static import static

from pagescanner.views import home_view, image_view, filter_view, four_point_projections_view

urlpatterns = [
    path('home/', home_view, name = 'home_view'),
    path('upload/', image_view, name = 'image_upload'),
    path('point_projections/', four_point_projections_view, name = 'four_point_projections_view'),
    path('filter/', filter_view, name = 'filter_view'),
    path(r'filter/(<file_path_list>\w+)/$', filter_view, name = 'filter_view'),
    path('admin/', admin.site.urls)
] + static(settings.STATIC_URL, document_root=[settings.STATIC_ROOT])


### create directory named media under root project before running the app
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)