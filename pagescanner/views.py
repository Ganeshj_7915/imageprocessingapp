from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.conf import settings
from django.urls import reverse
from .forms import ImageForm
import os
import time
from random import randint

# Create your views here.
def home_view(request, *args, **kwargs) :
    return render(request, "pagescanner/home_view.html")

def get_full_path(file_name) : 
    return os.path.abspath(file_name)

def image_view(request) :
    if request.method == "POST" :
        form = ImageForm(request.POST)
        if form.is_valid:
            files = request.FILES.getlist('my-file-selector')
            file_path_list = []
            for file in files :
                file_name = handle_uploaded_file(file, file.name)
                file_path_list.append(file_name)
            redirect = HttpResponseRedirect(reverse('filter_view'))
            file_path_list_as_string = ','.join(file_path_list)
            redirect['Location'] += '?file_path_list={}'.format(file_path_list_as_string)
            return redirect
            # return render(request, "pagescanner/filter_view.html", {'file_names' : file_path_list})
        else :
            print("Error: form is not valid")
    return HttpResponseRedirect(reverse('home_view'))

def filter_view(request):
    file_path_list = request.GET.get('file_path_list')
    print("file path list : {} and type : {}".format(file_path_list, type(file_path_list)))
    file_path_list = [get_file_access_path(file_name) for file_name in file_path_list.split(",")]
    return render(request, "pagescanner/filter_view.html", {'file_names' : file_path_list})

def four_point_projections_view(request):
    return render(request, "pagescanner/four_point_projections.html")

def handle_uploaded_file(file, file_name):
    file_name = str(randint(1, 10000)) + "_" + str(int(round(time.time() * 1000))) + "_" + file_name
    file_path = settings.MEDIA_ROOT / file_name
    with open(file_path, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return file_name

def get_file_access_path(file_name) :
    return settings.MEDIA_URL + file_name