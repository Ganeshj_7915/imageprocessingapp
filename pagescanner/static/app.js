function readURL(input) {   
    if (input.files) {
        for (file of input.files) {
            var reader = new FileReader();
            reader.readAsDataURL(file); // convert to base64 string
            reader.onload = function(e) {
                // TODO : To create elements dynamically for preview
                $("#upload-file-info").html(file.name);
                $('#preview').attr('src', e.target.result).width(400).height(400);
                $('#preview').attr('hidden', false);
                $('#confirm').attr('hidden', false);

                var dvPreview = document.getElementById("img_preview");
                var img = document.createElement("IMG");
                // img.className = "col-md-8 col-md-offset-4";
                img.height = "300";
                img.width = "300";
                img.src = e.target.result;
                dvPreview.appendChild(img);
            }
        }
    }
  }

function save() {
    var file_names = document.getElementById('file_names_details').value;
    console.log(typeof file_names);
    alert(file_names);
    // for (file_name of file_names) {
    //     var link = document.createElement('a');
    //     link.href = file_name;
    //     link.download = 'Download.jpg';
    //     document.getElementById('filter_image_list').appendChild(link)
    //     link.click();
    //     document.getElementById('filter_image_list').removeChild(link)
    // }
}

// dimensions will be populated of form (x,y, x1,y1, w, h)
// x, y -> start x and y axis which will be left top most part
// x1, y1 -> end x and y axis which will be right bottom most part
// w, h -> Total width and height of the image selected 
// eg : (x=29, y=37, x1=675 , y1=242, w=646, h=205)
var dimensions = []

function cropImage() {
    $('#filter_image').Jcrop({

        onSelect: function (c) { 
            size = { x: c.x, y: c.y,  
                    w: c.w, h: c.h };
            dimensions = [];
            dimensions.push(c.x, c.y, c.x2, c.y2, c.w, c.h); 
            console.log(dimensions);
        }
    });
}

